export interface IProduct {
    getPrice(): number;
    getName(): string;
}

export abstract class Product implements IProduct {

    constructor(protected name: string, protected price: number) {
    }

    public getPrice(): number {
        return this.price;
    }

    public getName(): string {
        return this.name;
    }
}

export abstract class OptionDecorator implements IProduct {

    constructor(protected product: IProduct, protected price: number, protected name: string = '') {
    }

    public getPrice(): number {
        return this.product.getPrice() + this.price;
    }

    public getName(): string {
        return this.product.getName() + (this.name ? `, ${this.name}` : '');
    }
}

