import { Product } from './base';

export class AdidasEnergyBoost extends Product {
    constructor() {
        super('Adidas Energy Boost', 699);
    }
}

export class NikeZoomPegasus extends Product {
    constructor() {
        super('Nike Zoom Pegasus', 349);
    }
}

export class NewBalanceZante extends Product {
    constructor() {
        super('New Balance Zante', 499);
    }
}

export class SauconyKinvara extends Product {
    constructor() {
        super('Saucony Kinvara', 399);
    }
}

export class HokaOneOne extends Product {
    constructor() {
        super('Hoka OneOne', 599);
    }
}

export class PumaFass600 extends Product {
    constructor() {
        super('Puma Fass 600', 299);
    }
}

export class AsicsGelNimbus extends Product {
    constructor() {
        super('Asics Gel Nimbus', 799);
    }
}

export class PopesKubota extends Product {
    constructor() {
        super('Papieskie Kuboty', 100);
    }
}