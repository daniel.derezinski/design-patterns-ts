export class UserBuilder {
    private _name: string;
    private _age: number;
    private _phone: string;
    private _address: string;

    constructor(name: string) {
        this._name = name;
    }

    public get name(): string {
        return this._name;
    }

    public setAge(value: number): this {
        this._age = value;
        return this;
    }

    public get age(): number {
        return this._age;
    }

    public setPhone(value: string): this {
        this._phone = value;
        return this;
    }

    public get phone(): string {
        return this._phone;
    }

    public setAddress(value: string): this {
        this._address = value;
        return this;
    }

    public get address(): string {
        return this._address;
    }

    public build(): User {
        return new User(this);
    }
}

export class User {
    private _name: string;
    private _age: number;
    private _phone: string;
    private _address: string;

    constructor(builder: UserBuilder) {
        this._name = builder.name;
        this._age = builder.age;
        this._phone = builder.phone;
        this._address = builder.address
    }

    public get name(): string {
        return this._name;
    }

    public get age(): number {
        return this._age;
    }

    public get phone(): string {
        return this._phone;
    }

    public get address(): string {
        return this._address;
    }

    public toString(): string {
        return `User name: ${this.name}, age: ${this.age}, phone: ${this.phone}, address: ${this.address}.`
    }
}


let u: User =
    new UserBuilder("Jancsi")
        .setAge(33)
        .setPhone("600-700-800")
        .setAddress("Kozie Boby 123")
        .build();

console.log(u.toString());