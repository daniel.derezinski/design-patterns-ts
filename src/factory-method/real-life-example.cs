using Adstream.MailService.Data.Bll;
using Adstream.MailService.Data.Bll.Interfaces;
using Adstream.MailService.Data.Mail;
using Adstream.MailService.Data.Message;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;

namespace Adstream.MailService.Data.Notification
{
    public class MessageProcessorFactory
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public NotificationMessage NotificationMessage { get; set; }
        public BaseMessage BaseMessage { get; set; }

        protected readonly IDataEntry m_dataEntryBll;
        protected readonly IUsersBll m_userBll;
        protected readonly IObjectSettingsBll m_objectSettingsBll;
        protected readonly IMailMessageSender m_mailMessageSender;
        protected readonly ITemplateBuilderBll m_tempalteBuilder;
        protected readonly IMailMessageCreator m_mailMessageCreator;
	    protected readonly ISettingsBll m_settingsBll;
	    protected readonly IFileDownloader m_FileDownloader;
        protected readonly IRetryQueueBll m_RetryQueueBll;


        public Models.ActionType ActionType { get; protected set; }

        public MessageProcessorFactory(NotificationMessage notificationMessage)
            : this(notificationMessage, new DataEntry(), new UsersBll(), new ObjectSettingsBll(),  new MailKitMessageSender(), new TemplateBuilderBll(), new RazorMailMessageCreator(), new SettingsBll(), new FileDownloader(), new RetryQueueBll())
        {
        }

        public MessageProcessorFactory(NotificationMessage notificationMessage, IDataEntry dataEntryBll, IUsersBll usersBll, IObjectSettingsBll objectSettingsBll, IMailMessageSender mailMessageSender, ITemplateBuilderBll templateBuilder, IMailMessageCreator mailMessageCreator, ISettingsBll settingsBll, IFileDownloader fileDownloader, IRetryQueueBll retryQueueBll)
        {
            this.NotificationMessage = notificationMessage;
            m_dataEntryBll = dataEntryBll;
            m_userBll = usersBll;
            m_objectSettingsBll = objectSettingsBll;
            m_mailMessageSender = mailMessageSender;
            m_tempalteBuilder = templateBuilder;
            m_mailMessageCreator = mailMessageCreator;
			m_settingsBll = settingsBll;
			m_FileDownloader = fileDownloader;
            m_RetryQueueBll = retryQueueBll;
        }

        public virtual List<IMessageProcessor> CreateProcessors()
        {
            var processors = new List<IMessageProcessor>();

            try
            {
                this.BaseMessage = JsonConvert.DeserializeObject<BaseMessage>(this.NotificationMessage.Message.Replace(Environment.NewLine, string.Empty));
                this.ActionType = m_dataEntryBll.GetActionTypeByName(this.BaseMessage.Action.Type);
	            if (this.ActionType == null)
	            {
					Logger.Warn("Invalid ActionType:{0}", this.BaseMessage.Action.Type);
		            return processors;
	            }
                if (UserMessageProcessor.IsProcessMessage(this.BaseMessage, this.ActionType))
                {
                    processors.Add(new UserMessageProcessor(this.NotificationMessage, m_dataEntryBll, m_userBll));
                }
                if (BusinessUnitMessageProcessor.IsProcessMessage(this.BaseMessage, this.ActionType))
                {
                    processors.Add(new BusinessUnitMessageProcessor(this.NotificationMessage, m_dataEntryBll, m_objectSettingsBll));
                }
                if (MailMessageProcessor.IsProcessMessage(this.BaseMessage, this.ActionType))
                {
                    processors.Add(new MailMessageProcessor(this.NotificationMessage, this.BaseMessage,  m_dataEntryBll, m_userBll, m_mailMessageSender, m_tempalteBuilder, m_mailMessageCreator, m_settingsBll, m_FileDownloader, m_RetryQueueBll));
                }

                if (AdHocMailMessageProcessor.IsProcessMessage(this.BaseMessage, this.ActionType))
                {
                    processors.Add(new AdHocMailMessageProcessor(this.NotificationMessage, this.BaseMessage, m_dataEntryBll, m_userBll, m_mailMessageSender, m_tempalteBuilder, m_mailMessageCreator, m_settingsBll, m_FileDownloader, m_RetryQueueBll));
                }

				if (InviteMailMessageProcessor.IsProcessMessage(this.BaseMessage, this.ActionType))
				{
					processors.Add(new InviteMailMessageProcessor(this.NotificationMessage, this.BaseMessage, m_dataEntryBll, m_userBll, m_mailMessageSender, m_tempalteBuilder, m_mailMessageCreator, m_settingsBll, m_FileDownloader, m_RetryQueueBll));
				}

            }
            catch (Exception exception)
            {
                Logger.Error(exception, "Error deserialize message: " + exception.ToString());
                return new List<IMessageProcessor>();
            }
            return processors;
        }
    }
}
