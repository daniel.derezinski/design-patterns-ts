import {
    AccountType,
    CurrentAccountInterestCalculation,
    HighRollerMoneyMarketInterestCalculation,
    InterestCalculationStrategy,
    MoneyMarketInterestCalculation,
    SavingsAccountInterestCalculation
} from '../startegy';

export class InterestCalculationStrategyFactory {

    private readonly currentAccountInterestCalculationStrategy = new CurrentAccountInterestCalculation();
    private readonly savingsAccountInterestCalculationStrategy = new SavingsAccountInterestCalculation();
    private readonly moneyMarketAccountInterestCalculationStrategy = new MoneyMarketInterestCalculation();
    private readonly highRollerMoneyMarketAccountInterestCalculationStrategy = new HighRollerMoneyMarketInterestCalculation();

    public getInterestCalculationStrategy(accountType: AccountType): InterestCalculationStrategy {
        switch (accountType) {
            case AccountType.CURRENT:
                return this.currentAccountInterestCalculationStrategy;

            case AccountType.SAVINGS:
                return this.savingsAccountInterestCalculationStrategy;

            case AccountType.STANDARD_MONEY_MARKET:
                return this.moneyMarketAccountInterestCalculationStrategy;

            case AccountType.HIGH_ROLLER_MONEY_MARKET:
                return this.highRollerMoneyMarketAccountInterestCalculationStrategy;

            default:
                return null;
        }
    }
}

export class InterestCalculator {

    private readonly interestCalculationStrategyFactory = new InterestCalculationStrategyFactory();

    public calculateInterest(accountType: AccountType, accountBalance: number): number {
        let interestCalculationStrategy = this.interestCalculationStrategyFactory.getInterestCalculationStrategy(accountType);
        if (interestCalculationStrategy != null) {
            return interestCalculationStrategy.calculateInterest(accountBalance);
        } else {
            return 0;
        }
    }
}

let types = [
    AccountType.CURRENT,
    AccountType.HIGH_ROLLER_MONEY_MARKET,
    AccountType.SAVINGS,
    AccountType.STANDARD_MONEY_MARKET,
];
let calc = new InterestCalculator();

console.log('---For 10 000 $---');
types.forEach(a => console.log(`Account: ${a}, Intrest: ${calc.calculateInterest(a, 10000)} $`));

console.log('---For 1 000 000 $---');
types.forEach(a => console.log(`Account: ${a}, Intrest: ${calc.calculateInterest(a, 1000000)} $`));