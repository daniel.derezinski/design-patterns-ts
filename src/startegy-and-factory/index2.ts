import {
    AccountType,
    CurrentAccountInterestCalculation,
    HighRollerMoneyMarketInterestCalculation,
    InterestCalculationStrategy,
    MoneyMarketInterestCalculation,
    SavingsAccountInterestCalculation
} from '../startegy';

export class NoInterestCalculation implements InterestCalculationStrategy {
    public calculateInterest(accountBalance: number): number {
        return 0;
    }
}

export class InterestCalculationStrategyFactory {

    private readonly currentAccountInterestCalculationStrategy = new CurrentAccountInterestCalculation();
    private readonly savingsAccountInterestCalculationStrategy = new SavingsAccountInterestCalculation();
    private readonly moneyMarketAccountInterestCalculationStrategy = new MoneyMarketInterestCalculation();
    private readonly highRollerMoneyMarketAccountInterestCalculationStrategy = new HighRollerMoneyMarketInterestCalculation();
    private readonly noInterestCalculationStrategy = new NoInterestCalculation()

    public getInterestCalculationStrategy(accountType: AccountType): InterestCalculationStrategy {
        switch (accountType) {
            case AccountType.CURRENT:
                return this.currentAccountInterestCalculationStrategy;

            case AccountType.SAVINGS:
                return this.savingsAccountInterestCalculationStrategy;

            case AccountType.STANDARD_MONEY_MARKET:
                return this.moneyMarketAccountInterestCalculationStrategy;

            case AccountType.HIGH_ROLLER_MONEY_MARKET:
                return this.highRollerMoneyMarketAccountInterestCalculationStrategy;

            default:
                return this.noInterestCalculationStrategy;
        }
    }
}

export class InterestCalculator {

    private readonly interestCalculationStrategyFactory = new InterestCalculationStrategyFactory();

    public calculateInterest(accountType: AccountType, accountBalance: number): number {
        return this.interestCalculationStrategyFactory.getInterestCalculationStrategy(accountType).calculateInterest(accountBalance);
    }
}

let calc = new InterestCalculator();
console.log(
    calc.calculateInterest(AccountType.STANDARD_MONEY_MARKET, 2000)
);