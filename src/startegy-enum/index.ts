import { InterestCalculationStrategy } from '../startegy';

export interface InterestCalculationStrategy {
    calculateInterest(accountBalance: number): number;
}

export class CurrentAccountInterestCalculation implements InterestCalculationStrategy {
    public calculateInterest(accountBalance: number): number {
        return accountBalance * (0.02 / 12);
    }
}

export class SavingsAccountInterestCalculation implements InterestCalculationStrategy {
    public calculateInterest(accountBalance: number): number {
        return accountBalance * (0.04 / 12);
    }
}

export class MoneyMarketInterestCalculation implements InterestCalculationStrategy {
    public calculateInterest(accountBalance: number): number {
        return accountBalance * (0.06 / 12);
    }
}

export class HighRollerMoneyMarketInterestCalculation implements InterestCalculationStrategy {
    public calculateInterest(accountBalance: number): number {
        return accountBalance < 100000.00 ? 0 : accountBalance * (0.075 / 12)
    }
}

export enum AccountType {
    CURRENT = 'CURRENT',
    SAVINGS = 'SAVINGS',
    STANDARD_MONEY_MARKET = 'STANDARD_MONEY_MARKET',
    HIGH_ROLLER_MONEY_MARKET = 'HIGH_ROLLER_MONEY_MARKET'
}

export type AccountTypeMap = {
    [P in AccountType]?: new () => InterestCalculationStrategy;
}

export const accountTypeMap: AccountTypeMap = {
    [AccountType.CURRENT]: CurrentAccountInterestCalculation,
    [AccountType.SAVINGS]: SavingsAccountInterestCalculation,
    [AccountType.STANDARD_MONEY_MARKET]: MoneyMarketInterestCalculation,
    [AccountType.HIGH_ROLLER_MONEY_MARKET]: HighRollerMoneyMarketInterestCalculation
};

export class InterestCalculator {

    public calculateInterest(accountType: AccountType, accountBalance: number): number {
        const calculator = accountTypeMap[accountType];
        if (calculator) {
            return new calculator().calculateInterest(accountBalance);
        }
        return 0;
    }
}

let calc = new InterestCalculator();
calc.calculateInterest(AccountType.HIGH_ROLLER_MONEY_MARKET, 1000);
