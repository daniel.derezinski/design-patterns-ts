import { Container } from "inversify";
import TYPES, { ThrowableWeapon, Warrior, Weapon } from "./contracts";
import { Ninja, Katana, Shuriken } from "./models";

var container = new Container();
container.bind<Warrior>(TYPES.Warrior).to(Ninja);
container.bind<Weapon>(TYPES.Weapon).to(Katana);
container.bind<ThrowableWeapon>(TYPES.ThrowableWeapon).to(Shuriken);

export default container;