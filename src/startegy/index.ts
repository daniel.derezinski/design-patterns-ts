export interface InterestCalculationStrategy {
    calculateInterest(accountBalance: number): number;
}

export class CurrentAccountInterestCalculation implements InterestCalculationStrategy {
    public calculateInterest(accountBalance: number): number {
        return accountBalance * (0.02 / 12);
    }
}

export class SavingsAccountInterestCalculation implements InterestCalculationStrategy {
    public calculateInterest(accountBalance: number): number {
        return accountBalance * (0.04 / 12);
    }
}

export class MoneyMarketInterestCalculation implements InterestCalculationStrategy {
    public calculateInterest(accountBalance: number): number {
        return accountBalance * (0.06 / 12);
    }
}

export class HighRollerMoneyMarketInterestCalculation implements InterestCalculationStrategy {
    public calculateInterest(accountBalance: number): number {
        return accountBalance < 100000.00 ? 0 : accountBalance * (0.075 / 12)
    }
}

export enum AccountType {
    CURRENT = 'CURRENT',
    SAVINGS = 'SAVINGS',
    STANDARD_MONEY_MARKET = 'STANDARD_MONEY_MARKET',
    HIGH_ROLLER_MONEY_MARKET = 'HIGH_ROLLER_MONEY_MARKET'
}

export class InterestCalculator {

    private readonly _currentAccountInterestCalculationStrategy = new CurrentAccountInterestCalculation();
    private readonly _savingsAccountInterestCalculationStrategy = new SavingsAccountInterestCalculation();
    private readonly _moneyMarketAccountInterestCalculationStrategy = new MoneyMarketInterestCalculation();
    private readonly _highRollerMoneyMarketAccountInterestCalculationStrategy = new HighRollerMoneyMarketInterestCalculation();

    public calculateInterest(accountType: AccountType, accountBalance: number): number {
        switch (accountType) {
            case AccountType.CURRENT:
                return this._currentAccountInterestCalculationStrategy.calculateInterest(accountBalance);

            case AccountType.SAVINGS:
                return this._savingsAccountInterestCalculationStrategy.calculateInterest(accountBalance);

            case AccountType.STANDARD_MONEY_MARKET:
                return this._moneyMarketAccountInterestCalculationStrategy.calculateInterest(accountBalance);

            case AccountType.HIGH_ROLLER_MONEY_MARKET:
                return this._highRollerMoneyMarketAccountInterestCalculationStrategy.calculateInterest(accountBalance);

            default:
                return 0;
        }
    }
}

let calc = new InterestCalculator();
calc.calculateInterest(AccountType.STANDARD_MONEY_MARKET, 1000);