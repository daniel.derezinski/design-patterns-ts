import { plan } from 'inversify/dts/planning/planner';

export interface Shape {
    draw(): void;
}

export class Rectangle implements Shape {

    public draw(): void {
        console.log('Rectangle.draw()');
    }
}

export class Circle implements Shape {

    public draw(): void {
        console.log('Circle.draw()');
    }
}

export class Line implements Shape {

    public draw(): void {
        console.log('Line.draw()');
    }
}

export class Square implements Shape {

    public draw(): void {
        console.log('Square.draw()');
    }
}


export class DrawingPalette {

    constructor(private rectangle: Rectangle, private circle: Circle, private line: Line, private sqauare: Square) {
    }

    public drawRectangle(): void {
        this.rectangle.draw();
    }

    public drawCircle(): void {
        this.circle.draw();
    }

    public drawLine(): void {
        this.line.draw();
    }

    public drawSquare(): void {
        this.sqauare.draw();
    }
}

export const palette = new DrawingPalette(new Rectangle(), new Circle(), new Line(), new Square());

palette.drawRectangle();
palette.drawCircle();
palette.drawLine();
palette.drawSquare();


// dobry przykład PAPI w Ads